# README

This is a small Go (_Golang_) app that sends out emails at specified intervals. It uses the _gocron_ package for scheduling tasks. Email is sent using the _net/smtp_ package, connected to a _Gmail_ account.

## App Information

App Name: go-email

Created: January 2020

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/go-email)

## Tech Stack

- Go
- _net/smtp_ package
- _gocron_ package
- _gotenv_ package
- Docker

## To Run

### Setup

1. Turn on Google "less secure app access" [here](https://myaccount.google.com/u/0/security)
2. Add .env file with necessary variables to project root

### Run via Terminal

1. Install Go locally
2. Set `$GOPATH`
3. Install dependencies:

```
$ cd go-email
$ go get -u "github.com/jasonlvhit/gocron"
$ go get -u "github.com/subosito/gotenv"
```

4. Start server

```
$ cd go-email
$ source run_dev.sh
```

### With Docker

```
$ docker-compose up -d --build
```

Last updated: 2025-01-19
