package scheduler

import (
  "../email"
  "../lib"

  "github.com/jasonlvhit/gocron"
)

func emailTask(m string) {
  lib.PrintMessage(m)
  msg := lib.ReportCurrentTime()
  email.SendEmail(msg)
}

func RunTasks() {
  gocron.Every(1).Minute().DoSafely(emailTask, "Every minute task")
  gocron.Every(1).Hour().From(gocron.NextTick()).DoSafely(emailTask, "Hourly task")
  gocron.Every(1).Day().At("08:00").DoSafely(emailTask, "Daily task")

  gocron.Start()
}
