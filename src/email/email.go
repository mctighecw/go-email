package email

import (
  "../conf"
  "../lib"

  "net/smtp"
)

type smtpServer struct {
  host string
  port string
}

func (s *smtpServer) Address() string {
  return s.host + ":" + s.port
}

func SendEmail(body string) {
  from := conf.APP_CONFIG.SenderEmail
  password := conf.APP_CONFIG.SenderPassword
  recipient := conf.APP_CONFIG.Recipient
  to := []string{recipient}

  messageStr := "From: " + from + "\n" +
    "To: " + recipient + "\n" +
    "Subject: Go Email\n\n" +
    body

  message := []byte(messageStr)

  // Note: Need to turn on Google "less secure app access"
  smtpServer := smtpServer{host: "smtp.gmail.com", port: "587"}
  auth := smtp.PlainAuth("", from, password, smtpServer.host)

  err := smtp.SendMail(smtpServer.Address(), auth, from, to, message)
  lib.LogError(err)

  lib.PrintMessage("Email has been sent")
}
