package server

import (
  "../conf"
  "../lib"
  "../scheduler"

  "io"
  "net/http"
  "time"
)

func CreateServer(port string) *http.Server {
  http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "text/html; charset=UTF-8")
    w.WriteHeader(http.StatusOK)

    m := conf.APP_CONFIG.Name + " is running " + "🚀"
    h := "<div style='font-family:Roboto, Arial, sans-serif;color:dodgerblue;'>" +
      m +
      "<div>"

    io.WriteString(w, h)
  })

  s := &http.Server{
    Addr: port,
    WriteTimeout: 15 * time.Second,
    ReadTimeout: 15 * time.Second,
  }

  scheduler.RunTasks()

  mode := lib.GetEnv("APP_ENV", "development")
  m := "Mode: " + mode
  lib.PrintMessage(m)

  return s
}
