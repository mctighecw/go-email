package main

import (
  "./conf"
  "./lib"
  "./server"

  "fmt"
  "log"
)

func main() {
  n := conf.APP_CONFIG.Name
  p := conf.APP_CONFIG.Port
  ps := fmt.Sprintf(":%d", p)
  s := server.CreateServer(ps)

  i := fmt.Sprintf("Starting %s on port %d", n, p)
  lib.PrintMessage(i)

  log.Fatal(s.ListenAndServe())
}
