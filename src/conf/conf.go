package conf

import (
  "../lib"

  "os"

  "github.com/subosito/gotenv"
)

type (
  AppConfig struct {
    Name string
    Port uint
    SenderEmail string
    SenderPassword string
    Recipient string
  }
)

var (
  APP_CONFIG *AppConfig

  SENDER_EMAIL string
  SENDER_PASSWORD string
  RECIPIENT string
)

func init() {
  mode := lib.GetEnv("APP_ENV", "development")

  if mode == "development" {
    gotenv.Load()
  }

  initConfig()
}

func initConfig() {
  SENDER_EMAIL = os.Getenv("SENDER_EMAIL")
  SENDER_PASSWORD = os.Getenv("SENDER_PASSWORD")
  RECIPIENT = os.Getenv("RECIPIENT")

  APP_CONFIG = &AppConfig {
    Name: "Go Email",
    Port: 8000,
    SenderEmail: SENDER_EMAIL,
    SenderPassword: SENDER_PASSWORD,
    Recipient: RECIPIENT,
  }
}
