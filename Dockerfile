FROM golang:1.13

WORKDIR /usr/src

RUN go get -u "github.com/jasonlvhit/gocron"
RUN go get -u "github.com/subosito/gotenv"

COPY . .

EXPOSE 8000

CMD ["go", "run", "./src/main.go"]
